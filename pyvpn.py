import argparse
import os
import re
import shutil
import sys
import time
from urllib.request import Request, urlopen
from urllib.error import HTTPError
import zipfile

FILE_VERSION = 5

VPN_LIST_URL = 'https://www.freeopenvpn.org/en/cf/{}.php'
COUNTRIES = [
    ('usa', 'USA'),
    ('korea', 'Korea'),
    ('japan', 'Japan'),
    ('russia', 'Russia'),
]
CONFIG_FOLDER = 'C:\\Program Files\\OpenVPN\\config'
VPN_URL = 'https://www.freeopenvpn.org/pservers/{}/'

FILE_URL = 'https://gitlab.com/TriLucas/PyVpnConfigLoader/-/raw/master/pyvpn.py'
ARCHIVE_URL = 'https://gitlab.com/TriLucas/PyVpnConfigLoader/-/archive/master/PyVpnConfigLoader-master.zip'

def download_file(src, target):
    response = urlopen(Request(src, headers={"User-Agent": "Chrome"}))
    with open(target, 'wb') as out_file:
        shutil.copyfileobj(response, out_file)

def get_members(zip):
    parts = []
    # get all the path prefixes
    for name in zip.namelist():
        # only check files (not directories)
        if not name.endswith('/'):
            # keep list of path elements (minus filename)
            parts.append(name.split('/')[:-1])
    # now find the common path prefix (if any)
    prefix = os.path.commonprefix(parts)
    if prefix:
        # re-join the path elements
        prefix = '/'.join(prefix) + '/'
    # get the length of the common prefix
    offset = len(prefix)
    # now re-set the filenames
    for zipinfo in zip.infolist():
        name = zipinfo.filename
        # only check files (not directories)
        if len(name) > offset:
            # remove the common prefix
            zipinfo.filename = name[offset:]
            yield zipinfo

def self_update(update=True):
    remote_file = fetch_url(FILE_URL)
    new_version = re.compile(r'FILE_VERSION = (\d+)').findall(remote_file)
    update = False

    if int(new_version[0]) > FILE_VERSION:
        update = True
        zip_path = None
        try:
            dest_folder = os.path.abspath(os.path.dirname(__file__))
            zip_path = os.path.join(dest_folder, 'tmp.zip')

            download_file(ARCHIVE_URL, zip_path)

            zip_ref = zipfile.ZipFile(zip_path, 'r')
            zip_ref.extractall(dest_folder, get_members(zip_ref))
            zip_ref.close()
        finally:
            if zip_path:
                os.remove(zip_path)

    if update:
        print('New version found! Restarting the downloader.')
        os.execvp("python", ["python", __file__,] + sys.argv[1:])
        return True
    return False

def fetch_url(url):
    req = Request(url, headers={"User-Agent": "Chrome"})
    response = urlopen(req)
    encoding = response.info().get_param('charset', 'utf8')
    return response.read().decode(encoding)

def find_configs():
    result = []
    for country, code in COUNTRIES:
        data = fetch_url(VPN_LIST_URL.format(country))
        match = re.compile(r'({}.*.ovpn)"'.format(VPN_URL.format(code)))
        result += match.findall(data)
    return result
    
def replace_files(data, folder, threshold):
    new_files = []
    #Download new configs first
    for item in data:
        dest_filename = os.path.basename(item)
        new_files.append(dest_filename)

        print('Downloading {}...'.format(dest_filename))

        dest = os.path.join(folder, dest_filename)
        download_file(item, dest)

    one_day = 60 * 60 * 24

    #Delete old configs
    exisiting = os.listdir(folder)
    for item in exisiting:
        full_path = os.path.join(folder, item)
        mtime = os.path.getmtime(full_path)
        now = time.mktime(time.localtime())

        if now - mtime > (one_day * threshold):
            print('Deleting outdated config ' + item)
            os.remove(os.path.join(folder, item))

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='''
DS Time for Maze!
Download the latest OpenVPN configuration files
''', formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument('-c', '--config-folder', dest='folder', default=CONFIG_FOLDER,
                        help='Folder where OpenVPN configuration files are stored.')
    parser.add_argument('-s', '--skip-update', dest='update', action='store_false',
                        default=True, help='Skip the self update')
    parser.add_argument('-t', '--threshold', dest='threshold', type=int, default=3,
                        help='Threshold (in days) that configs are kept')
    args = parser.parse_args()

    if args.update:
        updated = self_update()
    if not args.update or not updated:
        print("DS'n'Netflix time for Maze!")
        print('')
        print("###########################")
        config_links = find_configs()

        replace_files(config_links, args.folder, args.threshold)